<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 */

get_header(); ?>

 <div class="container">

   <h1>Page introuvable</h1>

   <div class="entry-content">
       <p>
         La page vers laquelle vous étiez supposé arriver semble ne pas exister... <br>
         L'équipe technique du site vient d'être informée de ce problème, nous faisons notre possible pour le résoudre.<br><br>
         En attendant, n'hésitez pas à découvrir notre site.
       </p>
   </div>

 </div>

 <?php get_footer(); ?>
