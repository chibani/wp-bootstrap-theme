/*global jQuery*/

jQuery(function(){
  jQuery('a[href$=".jpg"]').magnificPopup({
    type:'image',
    gallery:{
      enabled:true,
      /*navigateByImgClick: true,*/
      titleSrc: 'title',

      arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button

      tPrev: 'Précédente', // title for left button
      tNext: 'Suivante', // title for right button
      tCounter: '<span class="mfp-counter">%curr%&nbsp;sur&nbsp;%total%</span>' // markup of counter
    }
  });

});
