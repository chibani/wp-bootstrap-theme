/* Generated for WP-Bootstrap */
module.exports = function (grunt) {
  'use strict';
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  // 1. All configuration goes here
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //watch
    watch: {
      css_main: {
        files: [
          '../src/scss/main.scss',
          '../src/scss/variables.scss',
          '../src/scss/modules/*.scss',
          '../src/scss/zones/*.scss',
          '../src/scss/types/*.scss'
        ],
        tasks: ['sass:dev_main', 'concat_css', 'autoprefixer'],
        options: {
          livereload: false,
        },
      },
      css_bootstrap: {
        files: [
          '../src/scss/bootstrap.scss',
          '../src/scss/_bootstrap-variables.scss',
        ],
        tasks: ['sass:dev_bootstrap', 'concat_css', 'autoprefixer'],
        options: {
          livereload: false,
        },
      },
      css_ie: {
        files: [
          '../src/scss/ie.scss',
        ],
        tasks: ['sass:dev_ie', 'concat_css', 'autoprefixer'],
        options: {
          livereload: false,
        },
      },
      js: {
        files: '../src/js/**/*.js',
        tasks: ['copy:js'],
        options: {
          livereload: false,
        },
      },
      images: {
        files: '../src/images/**/*.{png,jpg,gif,svg,xml,json,ico}',
        tasks: ['copy:images'],
        options: {
          livereload: false,
        },
      },
      fonts: {
        files: '../src/fonts/*',
        tasks: ['copy:fonts'],
        options: {
          livereload: false,
        },
      },
    },
    // end watch
    //sass
    sass: { // Task
      dev_main: { // Target
        options: { // Target options
          style: 'nested',
        },
        files: { // Dictionary of files
          '../../static/css/main.css': '../src/scss/main.scss'
        }
      },
      dev_ie: { // Target
        options: { // Target options
          style: 'nested',
        },
        files: { // Dictionary of files
          '../../static/css/ie.css': '../src/scss/ie.scss'
        }
      },
      dev_bootstrap: { // Target
        options: { // Target options
          style: 'compact',
        },
        files: { // Dictionary of files
          '../../static/css/bootstrap.css': '../src/scss/bootstrap.scss'
        }
      },
      dist: { // Target
        options: { // Target options
          style: 'expanded',
          sourcemap: 'none',
        },
        files: { // Dictionary of files
          '../../static/css/bootstrap.css': '../src/scss/bootstrap.scss',
          '../../static/css/style.css': '../src/scss/style.scss',
          '../../static/css/ie.css': '../src/scss/ie.scss',
        }
      }
    },
    // end sass

    //auto prefixer
    autoprefixer: {
      options: {
        browsers: ['last 8 version', 'ie 8', 'ie 7']
      },
      // just prefix the specified file
      single_file: {
        options: {
          // Target-specific options go here.
        },
        src: '../../static/css/style.css',
        dest: '../../static/css/style.css'
      }
    },
    //end auto prefixer

    concat_css: {
      options: {
        // Task-specific options go here.
      },
      all: {
        src: [
          "../../static/css/bootstrap.css",
          "../../static/css/main.css"
        ],
        dest: "../../static/css/style.css"
      },
    },

    //css min
    cssmin: {
      minify: {
        expand: true,
        cwd: '../../static/css',
        src: ['*.css', '!*.min.css'],
        dest: '../../static/css',
        ext: '.css',
        // report: 'gzip'
      }
    },
    // end ccs min

    //notify
    notify: {
      done: {
        options: {
          title: 'Done!', // optional
          message: 'Whatever you were doing is done!', //required
        }
      },
      distStart: {
        options: {
          title: ' Prepping for distribution!', // optional
          message: 'Get ready for the awesome...', //required
        }
      },
      distDone: {
        options: {
          title: 'All packaged up!', // optional
          message: 'WP-Bootstrap is ready to be distributed ', //needed escaping!
        }
      },
      sassDone: {
        options: {
          title: ' Ta-da!!!', // optional
          message: 'Sass has compiled successfully ', //required
        }
      },
      initStart: {
        options: {
          title: 'Initializing project', // optional
          message: '...', //required
        }
      },
      initDone: {
        options: {
          title: 'Initialization done!', // optional
          message: 'Run : "grunt" and get to work!', //required
        }
      },
    },
    //endnotify
    //Bower copy
    bowercopy: {
      libs: {
        options: {
            destPrefix: '../../static/js'
        },
        files: {
            'modernizr.js': 'modernizr/modernizr.js',
            'jquery.js': 'jquery/jquery.js',
            'bootstrap.js': 'bootstrap-sass/assets/javascripts/bootstrap.js',
            'magnific-popup.js': 'magnific-popup/dist/jquery.magnific-popup.js'
        }
      },
      libsStyles: {
        options: {
            destPrefix: '../../static/css'
        },
        files: {
            'magnific-popup.css': 'magnific-popup/dist/magnific-popup.css'
        }
      }
    },
    //end Bower copy
    //copy
    copy: {
      js: {
        files: [
          // includes files within path
          {expand: true, cwd: '../src/js', src: ['*.js'], dest: '../../static/js', filter: 'isFile'},
        ]
      },
      images: {
        files: [
          // includes files within path
          {expand: true, cwd: '../src/images', src: ['**/*.{png,jpg,gif,svg,xml,ico,json}'], dest: '../../static/images', filter: 'isFile'},
        ]
      },
      fonts: {
        files: [
          // includes files within path
          {expand: true, cwd: '../src/fonts', src: ['**/*'], dest: '../../static/css/fonts', filter: 'isFile'},
        ]
      }
    },
    //end copy


    //Image min (for image compression)
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          src: ['../../static/images/**/*.{png,jpg,gif}'],
          dest: '../../static/images'
        }]
      }
    },
    //end image min

    // make a zipfile
    compress: {
      production: {
        options: {
          archive: '../../production/bootstrap-theme.zip'
        },
        files: [
          { expand: true, src: ['**/*',  '!dev/**', '!production/**'], cwd:'../../' },
        ]
      }
    },
    // end make a zipfile
    browserSync: {
      dev:{
        bsFiles: {
          src : [
            '../../static/css/*.css',
            '../../static/js/*.js',
            '../../**/*.php'
          ]
        },
        options: {
          proxy: '<%= php.dev.options.hostname %>:<%= php.dev.options.port %>',
          host: '127.0.0.1',
          notify: true,
          open: 'external',
          watchTask: true,
          reloadDebounce: 300,
          ghostMode: {
            clicks: true,
            scroll: true,
            links: true,
            forms: true
          }
        }
      }
  },
  php: {
    dev: {
      options: {
        hostname: '127.0.0.1',
        port: 8000,
        base: '../../../../../', // Project root
        keepalive: false,
        open: false,
        silent: true // prevent from verbosing
      }
    }
  }

  }); //end grunt package configs

  //Asset pipelines
  grunt.registerTask('prepJS',     [ 'copy:js' ]);
  grunt.registerTask('prepStyles', [ 'sass:dist', 'concat_css', 'autoprefixer', 'cssmin' ]);
  grunt.registerTask('prepImages', [ 'copy:images', 'imagemin:dynamic' ]);
  grunt.registerTask('prepFonts',  [ 'copy:fonts' ]);

  //RUN ON START
  grunt.registerTask('init',       ['notify:initStart', 'bowercopy', 'copy:js', 'copy:images', 'sass:dev_main', 'sass:dev_bootstrap', 'sass:dev_ie', 'concat_css','notify:initDone']);

  //RUN FOR PRODUCTION
  grunt.registerTask('prod',       ['notify:distStart', 'bowercopy', 'prepJS', 'prepImages', 'prepStyles', 'prepFonts', 'compress:production', 'notify:distDone']);

  //DEFAULT
  grunt.registerTask('default', ['php:dev', 'browserSync:dev', 'watch']);
};
