<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 */
?>

	<footer id="footer" class="source-org vcard copyright" role="contentinfo">
		<div class="container">
			<div class="row"  itemscope itemtype="http://schema.org/Organization">
				<div class="col-md-4">
					<div>
						<p><span itemprop="name">#NOM SOCIETE#</span></p>
						<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">12 rue de l'exemple</span><br><span itemprop="postalCode">84000</span> <span itemprop="addressLocality">Avignon</span></p>
						<p>Téléphone : <a href="tel:+33123456789" itemprop="telephone">01 23 45 67</a> <br>
						Email : <a href="mailto:contact@example.fr" itemprop="email">contact@EXEMPLE.FR</a></p>
					</div>
				</div>
				<div class="col-md-4">
					<p>
						Nos bureaux vous accueillent<br> du lundi au vendredi,<br> de 8:30 à 12:30, puis de 14:00 à 18:00
					</p>
				</div>
				<div class="col-md-4">
					<p>Retrouvez nous également sur les réseaux sociaux :</p>
					<ul>
						<link itemprop="url" href="<?php bloginfo('home') ?>">
						<li><a href="https://www.facebook.com/pages/#####" rel="no-follow" itemprop="sameAs">Facebook</a></li>
						<li><a href="https://plus.google.com/+#####/" rel="no-follow" itemprop="sameAs">Google+</a></li>
					</ul>
				</div>
			</div>
			<div class="text-center">
				<p>&copy; <?php echo date('Y') ?> <a href="<?php bloginfo('home') ?>"><?php bloginfo('name'); ?></a></p>
				<p><a href="/mentions-legales/" rel="no-follow">Mentions légales</a>, Conception : <a href="http://www.loicg.net/">LoïcG</a></p>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

</body>

</html>
