<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 */

    // Système d'options admin
    // Options Framework (https://github.com/devinsays/options-framework-plugin)
    if (!function_exists('optionsframework_init')) {
        define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/_inc/');
        require_once dirname(__FILE__) . '/_inc/options-framework.php';
    }

    if (!is_admin()) {
        require_once dirname(__FILE__).'/_inc/wp_bootstrap_pagination.php';
    }

    // Configuration du thème
    function bstheme_setup()
    {
        load_theme_textdomain('bstheme', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('structured-post-formats', array( 'link', 'video' ));
        //add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
        register_nav_menu('primary', 'Menu principal');
        add_theme_support('post-thumbnails');

        if (!is_admin()) {
            require_once(dirname(__FILE__).'/_inc/wp_bootstrap_navwalker.php');
        }
    }

    add_action('after_setup_theme', 'bstheme_setup');

    // Configuration des scripts (et styles)
    function bstheme_enqueue_scripts()
    {
        wp_enqueue_style('bstheme-styles', get_template_directory_uri() . '/static/css/style.css'); //our stylesheet
        wp_enqueue_style('magnific-popup-styles', get_template_directory_uri() . '/static/css/magnific-popup.css'); //our stylesheet

        //Bootstrap's js
        wp_enqueue_script('bootstrap-scripts', get_template_directory_uri() . '/static/js/bootstrap.js', array('jquery'), '1.0', true);
        wp_enqueue_script('magnific-popup-script', get_template_directory_uri() . '/static/js/magnific-popup.js', array('jquery'), '1.0', true);

        wp_enqueue_script('default-scripts', get_template_directory_uri() . '/static/js/footer.js', array('jquery'), '1.0', true);
        // if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
    }
    add_action('wp_enqueue_scripts', 'bstheme_enqueue_scripts');





    // Balise title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
    function bstheme_wp_title($title, $sep)
    {
        global $paged, $page;

        if (is_feed()) {
            return $title;
        }

//		 Add the site name.
        $title .= get_bloginfo('name');

//		 Add the site description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) {
            $title = "$title $sep $site_description";
        }

//		 Add a page number if necessary.
        if ($paged >= 2 || $page >= 2) {
            $title = "$title $sep " . sprintf(__('Page %s', 'bstheme'), max($paged, $page));
        }

        return $title;
    }
    add_filter('wp_title', 'bstheme_wp_title', 10, 2);




    // Menu principal
    register_nav_menu('primary', 'Menu principal');

    // Configuration de l'affichage des widgets
    if (function_exists('register_sidebar')) {
        function bstheme_widgets_init()
        {
            register_sidebar(array(
                'name'          => __('Sidebar Widgets', 'bstheme'),
                'id'            => 'sidebar-primary',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ));
        }
        add_action('widgets_init', 'bstheme_widgets_init');
    }


    // Posted On
    function posted_on()
    {
        printf(__('<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', ''),
            esc_url(get_permalink()),
            esc_attr(get_the_time()),
            esc_attr(get_the_date('c')),
            esc_html(get_the_date()),
            esc_attr(get_the_author())
        );
    }





// Configuration des tailles de miniatures
function bstheme_add_image_sizes()
{
    // add_image_size( 'bstheme-thumb', 300, 100, true );
        // add_image_size( 'bstheme-large', 600, 300, true );
    add_image_size('bstheme-contact-picture', 720, 9999);
    add_image_size('bstheme-realisationlist', 720, 480, ['center', 'center']);

    add_image_size('bstheme-full-width', 960, 9999);
}
add_action('init', 'bstheme_add_image_sizes');


function heGetImageUrl($path='')
{
    return heGetStaticFileUrl('images/'.$path);
}
function heGetStaticFileUrl($path='')
{
    return get_template_directory_uri().'/static/'.$path;
}

//Ajout options du thème
add_filter('of_options', function ($options) {
    /*$options[] = array(
            'name' => 'Afficher le ruban latéral ?',
            'desc' => 'Souhaitez-vous voir apparaitre le ruban ?',
            'id' => 'ribbon_show',
            'std' => '',
            'class' => '',
            'type' => 'checkbox'
    );

   $options[] = array(
       'name' => 'Texte du ruban latéral',
       'desc' => 'Un texte court, par exemple "Nous recrutons !".',
       'id' => 'ribbon_text',
       'std' => '',
       'class' => 'mini',
       'type' => 'text'
   );

    $options[] = array(
            'name' => 'URL du ruban latéral',
            'desc' => 'L\'URL du lien dans le ruban.',
            'id' => 'ribbon_url',
            'std' => bloginfo('home'),
            'class' => '',
            'type' => 'text'
    );*/

    $options[] = array(
            'name' => 'Téléphone',
            'desc' => 'N° de téléphone de la société.',
            'id' => 'phone',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'Adresse',
            'desc' => 'Adresse de la société.',
            'id' => 'address',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'Code postal',
            'desc' => 'Code postal de la société.',
            'id' => 'postcode',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'URL Facebook',
            'desc' => 'URL de la page Facebook officielle.',
            'id' => 'facebook_url',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'URL Google+',
            'desc' => 'URL de la page Google+.',
            'id' => 'googleplus_url',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'URL Twitter',
            'desc' => 'URL de la page Twitter officielle.',
            'id' => 'twitter_url',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'URL Instagram',
            'desc' => 'URL de la page Instagram officielle.',
            'id' => 'instagram_url',
            'std' => '',
            'class' => '',
            'type' => 'text'
    );

    $options[] = array(
            'name' => 'URL Mentions légales',
            'desc' => 'URL des mentions légales.',
            'id' => 'legalnotes_url',
            'std' => 'mentions-legales',
            'class' => '',
            'type' => 'text'
    );

   return $options;
});

// Remplace le comportement de la galerie WP par une galerie compatible Bootstrap
add_shortcode('gallery', 'bstheme_shortcode_gallery');

// Replace with custom shortcode
function bstheme_shortcode_gallery($attr)
{
    $post = get_post();

    static $instance = 0;
    $instance++;

    if (!empty($attr['ids'])) {
        if (empty($attr['orderby'])) {
            $attr['orderby'] = 'post__in';
        }
        $attr['include'] = $attr['ids'];
    }

    $output = apply_filters('post_gallery', '', $attr);

    if ($output != '') {
        return $output;
    }

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby']) {
            unset($attr['orderby']);
        }
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => '',
        'icontag' => '',
        'captiontag' => '',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'link' => '',
        'exclude' => ''
                    ), $attr));

    $id = intval($id);

    if ($order === 'RAND') {
        $orderby = 'none';
    }

    if (!empty($include)) {
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif (!empty($exclude)) {
        $attachments = get_children(array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    } else {
        $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    }

    if (empty($attachments)) {
        return '';
    }

    if (is_feed()) {
        $output = "\n";
        foreach ($attachments as $att_id => $attachment) {
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        }
        return $output;
    }

    //Bootstrap Output Begins Here
    //Bootstrap needs a unique carousel id to work properly. Because I'm only using one gallery per post and showing them on an archive page, this uses the $post->ID to allow for multiple galleries on the same page.

    $output .= '<div id="carousel-' . $post->ID . '" class="carousel slide" data-ride="carousel">';
    $output .= '<!-- Indicators -->';
    $output .= '<ol class="carousel-indicators">';

    //Automatically generate the correct number of slide indicators and set the first one to have be class="active".
    $indicatorcount = 0;
    foreach ($attachments as $id => $attachment) {
        if ($indicatorcount == 1) {
            $output .= '<li data-target="#carousel-' . $post->ID . '" data-slide-to="' . $indicatorcount . '" class="active"></li>';
        } else {
            $output .= '<li data-target="#carousel-' . $post->ID . '" data-slide-to="' . $indicatorcount . '"></li>';
        }
        $indicatorcount++;
    }

    $output .= '</ol>';
    $output .= '<!-- Wrapper for slides -->';
    $output .= '<div class="carousel-inner">';
    $i = 0;

    //Begin counting slides to set the first one as the active class
    $slidecount = 1;
    foreach ($attachments as $id => $attachment) {
        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

        if ($slidecount == 1) {
            $output .= '<div class="item active">';
        } else {
            $output .= '<div class="item">';
        }

        $image_src_url = wp_get_attachment_image_src($id, $size);
        $output .= '<img src="' . $image_src_url[0] . '">';
        $output .= '    </div>';


        if (trim($attachment->post_excerpt)) {
            $output .= '<div class="caption hidden">' . wptexturize($attachment->post_excerpt) . '</div>';
        }

        $slidecount++;
    }

    $output .= '</div>';
    $output .= '<!-- Controls -->';
    $output .= '<a class="left carousel-control" href="#carousel-' . $post->ID . '" data-slide="prev">';
    $output .= '<span class="glyphicon glyphicon-chevron-left"></span>';
    $output .= '</a>';
    $output .= '<a class="right carousel-control" href="#carousel-' . $post->ID . '" data-slide="next">';
    $output .= '<span class="glyphicon glyphicon-chevron-right"></span>';
    $output .= '</a>';
    $output .= '</div>';
    $output .= '</dl>';
    $output .= '</div>';

    return $output;
}
