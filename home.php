<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 */
 get_header(); ?>

 <div class="container">

  <section class="row">

    <div class="w80p col-lg-10 col-lg-offset-1">
      <h2 class="text-center">Lorem ipsum ?</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </div>

    <div class="w80p col-md-3">
      <h2 class="text-center">Encore plus ?</h2>
      <p>
        <ul>
          <li>Nous sommes cools</li>
          <li>On travaille vite</li>
          <li>Importateur exclusif de code</li>
          <li>On fait de super gâteaux</li>
          <li>On carbure au café</li>
        </ul>
        <br><a href="#"> Découvrez nos réalisations !</a>
      </p>
    </div>

    <div class="col-md-9">
      <h2 class="text-center">Mais alors ?</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </div>


  </section>


 </div><!-- /.container -->

<?php get_footer(); ?>
