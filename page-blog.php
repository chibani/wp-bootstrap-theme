<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 *
 * Template Name: Page d'accueil "blog" (liste d'articles)
 */
 get_header(); ?>

 <div class="container posts-list">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h2><?php the_title(); ?></h2>
      <div class="posts-list-intro">
        <?php the_content(); ?>
      </div>


        <?php
        $c_query = new WP_Query(
          array(
            // 'posts_per_page'=>1,
            'paged'=>(get_query_var('paged')) ? get_query_var('paged') : 1,
          )
        );

        $pn = 0;
        while($c_query->have_posts()) :
          $pn++;
          $c_query->the_post();

          ?>


          <?php if($pn===1 || (($pn-1)%3==0)): ?>
            <div class="row">
          <?php endif; ?>

          <article class="col-md-4">
            <h2><a href = "<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
            <div class="meta text-right">Publié le <time datetime="<?php echo $post->post_date ?>"><?php echo date_i18n( get_option( 'date_format' ), strtotime($post->post_date)); ?></time></div>
            <?php the_post_thumbnail('homeevolution-contact-picture', array('class'=>'img-responsive') ); ?>
            <div class="post-excerpt"><?php the_content('<br>Lire la suite...') ?></div>
          </article>

          <?php if($pn == $c_query->post_count || ($pn%3===0) ): ?>
            </div>
          <?php endif; ?>


        <?php
        endwhile;?>

    	<?php
      wp_bootstrap_pagination(
        array(
          'custom_query'=>$c_query,
          'previous_string'=>'Pr&eacute;c&eacute;dentes',
          'next_string'=>'Suivantes',
          'first_string'=>'Premier',
          'last_string'=>'Derni&eagrave;re',

        )
      );
      // echo paginate_links(array(
      //   'current' => max(1, get_query_var('paged')),
      //   'total' => $published_posts,
      // ));

      wp_reset_postdata();
      ?>

	<?php endwhile; ?>


	<?php else : ?>

		<h2><?php _e('Nothing Found','homeevolution'); ?></h2>

	<?php endif; ?>

</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
