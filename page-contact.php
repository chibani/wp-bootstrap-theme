<?php

/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 *
 * Template Name: Contact
 */
get_header(); ?>

<div class="container">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h1><?php the_title() ?></h1>

    <div class="entry-content">
        <?php the_content(); ?>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-push-6 col-sm-5 col-sm-push-7">
            <?php the_post_thumbnail( 'homeevolution-contact-picture', array('class'=>'img-responsive') ); ?>
        </div>

        <div class="col-md-6 col-md-pull-6 col-sm-7 col-sm-pull-5">
            <?php echo do_shortcode('[contact-form-7 id="4" title="Formulaire de contact 1"]' ); ?>
        </div>
    </div>

  <?php endwhile;endif; ?>
</div>

<?php get_footer(); ?>
