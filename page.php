<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 */

get_header(); ?>

 <div class="container">
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
     <div class="text-center">
       <?php the_post_thumbnail( 'homeevolution-full-width', array('class'=>'img-responsive') ); ?>
     </div>

     <h1><?php the_title() ?></h1>

     <div class="entry-content">
         <?php the_content(); ?>
     </div>

   <?php endwhile;endif; ?>
 </div>

 <?php get_footer(); ?>
