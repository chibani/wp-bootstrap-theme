<?php
/**
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 1.0
 */
 get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class('container') ?> id="post-<?php the_ID(); ?>">

			<h1 class="entry-title"><?php the_title(); ?></h1>
      <!-- <?php the_post_thumbnail('homeevolution-full-width', array('class'=>'img-responsive') ); ?>  -->

			<div class="entry-content">

				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => __('Pages: ','homeevolution'), 'next_or_number' => 'number')); ?>

				<?php //the_tags( __('Tags: ','homeevolution'), ', ', ''); ?>

				<?php //posted_on(); ?>

			</div>

			<?php edit_post_link(__('Edit this entry','homeevolution'),'','.'); ?>

		</article>

	<?php //comments_template(); ?>

	<?php endwhile; endif; ?>

<?php post_navigation(); ?>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
